from django.db.models.signals import post_save
from django.dispatch import receiver

from movies import tasks
from movies.models import Movie
from movies.utils import send_email_to_people


@receiver(post_save, sender=Movie)
def tell_people(sender, instance, created, **kwargs):
    if created:
        message = f'{instance.title}, Created'
        tasks.send_email_task.delay(message=message)


