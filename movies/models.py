from django.db import models

# Create your models here.


class Movie(models.Model):
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE, related_name='genre')

    def __str__(self):
        return f'{self.title}'


class Genre(models.Model):
    title = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.title}'