from django.urls import path
from . import views

app = 'movies'

urlpatterns = [
    path('search/', views.SearchMovieView.as_view(), name='search'),
    ]
