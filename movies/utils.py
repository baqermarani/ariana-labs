from django.contrib.auth.models import User
from django.core.mail import send_mail


def send_email_to_people(message):
    from_email = 'a@c.cpm'
    recipient_list = User.objects.all().values_list('email')
    send_mail(subject='News', message=message, from_email=from_email,
              recipient_list=recipient_list)
