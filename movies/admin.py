from django.contrib import admin

# Register your models here.
from movies.models import Movie, Genre


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ['title', ]
    list_filter = ['title', 'genre', ]
    sortable_by = ['title', ]


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ['title']
