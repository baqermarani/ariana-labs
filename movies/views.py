import json
from rest_framework.response import Response
from rest_framework.views import APIView
from .documents import MovieDocument
# Create your views here.


class SearchMovieView(APIView):
    def get(self, request):
        search_keyword = request.GET['search_keyword']
        search = list(MovieDocument.search().suggest('suggestions', search_keyword, term={'field': 'title'}).execute())
        return Response(data=json.dumps(search))
