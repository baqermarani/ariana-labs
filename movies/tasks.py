from celery import shared_task
from .utils import send_email_to_people


@shared_task
def send_email_task(message):
    send_email_to_people(message=message)
